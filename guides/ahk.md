---
layout: default
title: How to set up AutoHotkey (AHK)
permalink: /guides/ahk
---

# What is AutoHotkey (AHK)?

[AutoHotkey](https://www.autohotkey.com/), or "AHK" for short, is a tool to create simple scripts in order to automate certain things. Possible use-cases are: [autoclickers](/guides/autoclicker), [automation of easy repetetive grind](https://steamcommunity.com/sharedfiles/filedetails/?id=2010047308) and many more. 

# Installation and first steps

First click on this [link](https://www.autohotkey.com/) to get to the official website for AutoHotkey, click on the Download-Button, and then choose "Download Current Version". Afterwards, simply click the just downloaded .exe and choose "Express Installation" during the setup. As soon as the installation is done, you can now click on "Exit" and right-click on the desktop or any other folder and find the option to create an "AutoHotkey Script" in the "New" context menu. After creating a new AHK Script, you can now rename the script to whatever you want, e.g. "myScript.ahk", and right-click it and choose "Edit Script" if you want to edit it. For the purpose of this guide, we will now create a simple script to give you an idea what a script could look like. The whole content of the simple script consists of:
```autohotkey
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

^h::
Send, Hello World
return
```
So what does each line do exactly? The first four lines are generated automatically and can be left there as they are (they are already explained in the comments next to them). 
The first line, which we added ourselves is: ```^h::```, which represents the hotkey to later start our script. Anything that is on the left side of the ```::``` is what you need to press, which in this case is ```^h```, where ```^``` corresponds to the Ctrl-Key and ```h``` to the letter H. The second line, which we added ourselves is: ```Send, Hello World``` and as the "Send" suggests it sends the keystrokes following the comma in this line. In light of this, as soon as this line is executed, the keystrokes "Hello World" will be sent. The third line, which we added ourselves is: ```return``` and all that is done in this line is that it stops the script and prevents the code from going any further than this line as soon as it gets executed. In conclusion, what the script does, is simply output the keystrokes "Hello World" as soon as you press Ctrl+H.

After you created the abovementioned AHK script you can now go ahead and save the file. You can now start the script by simply double-clicking the script. To test if everything works you can now open any editor and hit Ctrl+H, which should automatically write the letters "Hello World". To end the script or pause it you can simply right click the corresponding icon in the taskbar and select "Pause Script" or "Exit".

# Creating your first actually useful script

So far all you did with this script is simply output the letters "Hello World", which by itself is not very useful. However, with this tool you can actually create very useful scripts. For a guide on how to create your own simple autoclicker with AHK you can simply read [this guide](/guides/autoclicker). For a more advanced script to see what is possible with AHK you can check out the [automation of job level grinding for Final Fantasy 3](https://steamcommunity.com/sharedfiles/filedetails/?id=2010047308) that Jippen created in his steam guide.
