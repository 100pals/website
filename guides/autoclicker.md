---
layout: default
title: How to create your own autoclicker
permalink: /guides/autoclicker
---

# How to create your own autoclicker

If you already installed AutoHotkey(AHK) and you know some basics, you are good to go, otherwise please check out the other guide on: [How to set up AHK and first steps](/guides/ahk) first. First, create a script called autoclicker.ahk (or whatever you want to call it). The basic script of a simple autoclicker consists of the following:
```autohotkey
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#MaxThreadsPerHotkey 3

F8::	;choose the hotkey you want here
Toggle := !Toggle
Loop
{
	If (!Toggle)
		Break
	Click
	Sleep 10	;milliseconds between clicks, Sleep x equals to 1000/x clicks per second
}
return
```
The hotkey to start the script is set to F8 in line 8, but you can choose any hotkey you like, e.g., ```^c``` for ```Ctrl+C``` or ```F1``` for ```F1```. What this script does by clicking the hotkey is simply click at the current mouse position after 10ms until the hotkey is clicked again. If you want to have a faster / slower autoclicker you can simply adjust the clicking speed by changing the Sleep value at line 15, where the value corresponds to the millisecond value. This means that ```Sleep x``` corresponds to 1000/x clicks per second. As an additional note, some games have a max value for clicks per second as an input, so for those cases you can set the value to this max value / slightly below.
