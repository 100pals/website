---
layout: default
title: Make A Merge Request
permalink: /guides/make_a_mr
---

# How to Make A Merge Request

So you want to propose an edit the [AchievementHunting.com](/) website, or propose a change to the [Unified Achievement Hunting Rules](/rules). Well, these are both feasable!

To start off, you'll have to visit the gitlab repository for either the [website](https://gitlab.com/100pals/website) or the [unified rules](https://gitlab.com/100pals/unified_hunting_rules).

The first step is to click the "Web IDE" button as seen here.

![](/images/guides/make_a_mr/make_a_mr_1.png)

And most likely, you'll hit a popup asking you to fork the repository. As this is a technical requirement, click the green button.

![](/images/guides/make_a_mr/make_a_mr_2.png)

Next, choose the file you want to change. For the unified rules, the only file to worry about is `index.md`, but for the website, the file is whatever `*.md` file relates to the path. This file, for example, is `guides/make_a_mr.md`

Edit the file as you wish. This site is built with a tool called [Jekyll](https://jekyllrb.com/), and uses a format called [MarkDown](https://www.markdownguide.org/cheat-sheet). If you're not sure how to do something, then please ask Jippen, or leave a comment on your pull request asking for help.

Finally, click the "Commit..." button to start the merge request process

![](/images/guides/make_a_mr/make_a_mr_3.png)

On the next screen, you'll need to give a brief description, create a new branch (optionally with a helpful name), MAKE SURE to check the "Start a new merge request" box, then click the green button to commit the change.

![](/images/guides/make_a_mr/make_a_mr_4.png)

Your changes are now saved to your fork of the repository (that we made in the first step), but you havn't yet made a request to us to actually have the changes made to the live version of AchievementHunting.com. To fix this, we must open a merge request (also sometimes called a pull request).

For unified rule changes, we want to preserve a copy of the chat logs for referencing in the future. So paste those in as a comment.

Then, check all the boxes, but be VERY SURE to check the box in the contribution section as shown in the picture. This allows members of our staff to edit your request, in case we need to fix any typos, links, or other issues with the requested changes.

![](/images/guides/make_a_mr/make_a_mr_5.png)

Finally, and only AFTER the merge request has been accepted - you may wish to make another change. However, your fork is now no longer the same as the master repository we are maintaining. You can either delete your fork and recreate it AFTER the last merge request is accepted in... or you can follow [this guide to set up automatic mirroring](https://forum.gitlab.com/t/refreshing-a-fork/32469).

WARNING: If you do the 2nd step, then you must be very careful to never edit the `master` branch of your repository. The place you need to be careful is in the 4th step above - where you were instructed to make a new branch, you MUST always make a new branch and NEVER edit the master branch

![](/images/guides/make_a_mr/warning.png)