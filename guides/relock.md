---
layout: default
title: Relocking Guide
permalink: /guides/relock
redirects:
  - /relock
---

# How to relock your achievements

Relocking your achievements can be done from within steam itself. The first step is to open the steam console. To do so, simply click this link: <steam://open/console>

For windows users, you can also press Win + R to open up the Run menu, and type `steam://open/console` there, which should have the same effect.

You can also modify your steam shortcut to add `--console` to the steam command, and it will enable the console in your menu. 

Either way, you should come to a screen that looks like this:

![](/images/guides/relock/steam_console.png)

Even though this looks empty - thats correct. You can confirm that the console is working by clicking into the lighter area at the bottom of the screen and typing the command `app_info_print 440`, which should dump a ton of messages onto your screen

![](/images/guides/relock/steam_console_appinfo.png)


# Find the information for your game

Next step is to find your game's app ID, and the API names of the achievements you want to reset. The easiest way to do this is to search for the game on [SteamDB](https://steamdb.info). 

In this example, we can see that the App ID is 440, and the API name for the first achievement is `TF_PLAY_GAME_EVERYCLASS`

![](/images/guides/relock/steamdb.png)

# Reset an entire game

Resetting an entire game is pretty straightforward. Back in the steam console, simply type the command `reset_all_stats 440` and it will reset all of your achievements and tracking stats as though you hadn't played the game before.

You will likely want to also clear the savegames for your game to prevent achievements from repopping - see that section below.

# Reset specific achievements

If you want to reset just a specific achievement, then you will need the API name of the achievement as well. In this case, if we wanted to reset the "Head of the Class" achievment in TF2 that we looks at earlier, we would run the following command in the console:

`achievement_clear 440 TF_PLAY_GAME_EVERYCLASS`

This will reset that achievement. However, if this achievement pops back on startup even after clearing your savegames and resetting again - it may be due to the achievement tracking stats feature in steam. Unfortunately, the only way to fix that via the steam console is to reset the entire game as per the previous section.

# Clearing Savegames

Usually, you will also need to clear your related savegames, as games will often re-pop achievements when your savegame is loaded.

For this, we recommend checking [PC Gaming Wiki](https://pcgamingwiki.com) to find the save game location. For the example above of 100% Orange Juice, lets check the related wiki page:

![](/images/guides/relock/pcwiki.png)

Here we can see the save location is the file `user.dat` in the game install folder, and the game has steam cloud support.

So, to remove the save game, a few steps need to happen. First off, open the properties for this game, and disable the steam cloud saving feature for it.

![](/images/guides/relock/steam1.png)

Under the `Updates` tab, uncheck the `Enable Steam Cloud synchronization for 100% Orange Juice` checkbox, and then hit Close.

![](/images/guides/relock/steam2.png)

Then, go to the location of the savegames, and delete the save file(s). NOTE: this may also involve editing the registry, depending on the game.

Launch the game, and ensure your relocked achievements do not re-unlock.

If it doesn't, then start a new game, and create a new save. You can then turn steam cloud saving back on, at which point on the next game launch, steam will say your games are desynced. Choose to overwrite the steam cloud with what's on your local computer, and you're done!

If you do get an achievement re-pop, then please leave a message on your appeal indicating this, so further instructions can be provided.
