// Convinience function to allow collapseable rules that open when linked to
function openTarget() {
  var hash = location.hash.substring(1);
  if(!hash) {return;} 
  var link = document.querySelector("details h3#"+hash+" a[href='#"+hash+"']");
  var details = link.closest("details");
  details.open = true;
}

function find(e) {
    if (e.ctrlKey && e.keyCode == 70) {
        openAll();
    }
}

function openAll() {
    var x = document.getElementsByTagName("details");
    var i;
    for (i = 0; i < x.length; i++) {
         x[i].setAttribute("open", "true");
    }
}

document.addEventListener('keydown', find);
window.addEventListener('hashchange', openTarget);
window.addEventListener('load', openTarget);